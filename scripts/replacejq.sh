#!/bin/bash
if [ -z "$1" ]
  then
    echo "No argument supplied"
    echo "Use $0 filename.json to replace content inside json file and disable ELK query for dashboard"
    exit 1
fi
NAME=$1
TMP=$(mktemp /tmp/elk-script.XXXXXX)
PWD=$(pwd)
echo "processing $TMP in $PWD with file $NAME"
cat "$NAME" | jq '.rows[].panels[].editable = false' | jq '.editable=false' | jq '.rows[].editable|=false' | jq '.pulldowns[0].enable = false' | jq '.loader.save_elasticsearch=false' > $TMP
echo "moving $TMP to $NAME"
mv -f "$TMP" "$NAME" 
